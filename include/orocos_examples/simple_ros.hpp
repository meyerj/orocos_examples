/* Copyright (c) 2020 Houston Mechatronics Inc.
 *
 * Distribution of this file or its parts, via any medium is strictly
 * prohibited. Permission to use must be explicitly granted by Houston
 * Mechatronics Inc.
 */

#ifndef OROCOS_EXAMPLES_SIMPLE_ROS_HPP
#define OROCOS_EXAMPLES_SIMPLE_ROS_HPP

#include <rtt/Component.hpp>
#include <rtt/RTT.hpp>
#include <rtt_roscomm/rosservice.h>
#include <rtt_roscomm/rostopic.h>
#include <rtt_rosparam/rosparam.h>
#include <std_msgs/Int32.h>
#include <oe_msgs/Operation.h>

namespace orocos_examples
{
class SimpleRos : public RTT::TaskContext
{
  public:
    explicit SimpleRos(const std::string& name);
    virtual ~SimpleRos();

    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();

    bool connect_to_topics;

    int property_;
    int input_port_value_;
    int event_port_value_;
    int event_port_cb_value_;
    int op_callback_value_;

    // These ports are `int` ports, and will automatically map to `std_msgs/Int32` when connected to a ROS topic
    RTT::InputPort<int> input_port_;
    RTT::OutputPort<int> output_port_;

    // These ports are `std_msgs::Int32` to show that you can use ROS messages, too.
    RTT::InputPort<std_msgs::Int32> event_port_;
    RTT::InputPort<std_msgs::Int32> event_port_cb_;
    void eventPortCallback();

    // Operations and OperationCallers that are tied to ROS services must share the same callback function definition as ROS services, though.
    RTT::OperationCaller<bool(oe_msgs::Operation::Request&, oe_msgs::Operation::Response&)> op_caller_;
    bool op_callback(oe_msgs::Operation::Request&, oe_msgs::Operation::Response&);
};
}  // namespace orocos_examples

#endif
