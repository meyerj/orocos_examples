/* Copyright (c) 2020 Houston Mechatronics Inc.
 *
 * Distribution of this file or its parts, via any medium is strictly
 * prohibited. Permission to use must be explicitly granted by Houston
 * Mechatronics Inc.
 */

#include <boost/program_options.hpp>
#include <iostream>
#include <rtt/os/main.h>
#include <ocl/DeploymentComponent.hpp>
#include <ocl/TaskBrowser.hpp>
#include <rtt/internal/GlobalService.hpp>
#include <ros/ros.h>

int ORO_main(int argc, char* argv[])
{
    std::string component_name;
    boost::program_options::options_description description("Simple application for executing SimpleRos.");

    description.add_options()("help,h", "Display help options")
        /* ^ the help option */
        /* _ the name option */
        ("name,n", boost::program_options::value<std::string>(&component_name)->required()->default_value("simple"), "The name of the RTT component and ROS namespace.");

    boost::program_options::variables_map command_line_args;

    try
    {
        boost::program_options::store(boost::program_options::parse_command_line(argc, argv, description), command_line_args);
        boost::program_options::notify(command_line_args);
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        std::cout << description << std::endl;
        return 1;
    }

    if (command_line_args.count("help"))
    {
        std::cout << description << std::endl;
        return 0;
    }

    RTT::Service::shared_ptr global_service = RTT::internal::GlobalService::Instance();
    if (not global_service)
    {
        std::cerr << "Could not aquire pointer to RTT GlobalService." << std::endl;
        return 1;
    }

    OCL::DeploymentComponent deployer;

    if (not deployer.import("rtt_ros"))
    {
        std::cerr << "Orocos package orocos_examples could not be imported." << std::endl;
        return 1;
    }

    if (not global_service->provides("ros"))
    {
        std::cerr << "Global Service could not find [ros] service." << std::endl;
        return 1;
    }

    RTT::OperationCaller<bool(std::string)> ros_import = global_service->provides("ros")->getOperation("import");

    if (not ros_import("orocos_examples"))
    {
        std::cerr << "Failed to import [orocos_examples]." << std::endl;
        return 1;
    }

    if (not deployer.loadComponent(component_name, "orocos_examples::SimpleRos"))
    {
        std::cerr << "Failed to load [orocos_examples::SimpleRos]." << std::endl;
        return 1;
    }

    if (not deployer.loadService(component_name, "rosparam"))
    {
        std::cerr << "Failed to load [rosparam] service in [orocos_examples::SimpleRos]." << std::endl;
        return 1;
    }

    if (not deployer.loadService(component_name, "rosservice"))
    {
        std::cerr << "Failed to load [rosservice] service in [orocos_examples::SimpleRos]." << std::endl;
        return 1;
    }

    RTT::TaskContext* simple_ros = deployer.getPeer(component_name);
    if (not simple_ros)
    {
        std::cerr << "Could not aquire pointer to [" << component_name << "]." << std::endl;
        return 1;
    }

    if (not simple_ros->configure())
    {
        std::cerr << "Could not configure [" << component_name << "]." << std::endl;
        return 1;
    }

    if (not simple_ros->start())
    {
        std::cerr << "Could not start [" << component_name << "]." << std::endl;
        return 1;
    }

    if (not simple_ros->setPeriod(0.5))
    {
        std::cerr << "Could not set period for [" << component_name << "] to [0.5]." << std::endl;
        return 1;
    }

    // Main event loop.
    OCL::TaskBrowser browser(simple_ros);
    browser.loop();

    deployer.kickOutAll();
    deployer.shutdownDeployment();

    return 0;
}
