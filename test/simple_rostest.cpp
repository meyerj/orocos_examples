/*
 * Copyright (c) 2020 Houston Mechatronics Inc.
 *
 * Distribution of this file or its parts, via any medium is strictly
 * prohibited. Permission to use must be explicitly granted by Houston
 * Mechatronics Inc.
 */

#include <gtest/gtest.h>
#include <ros/ros.h>
#include <ros/master.h>
#include <std_msgs/Int32.h>
#include <oe_msgs/Operation.h>

std_msgs::Int32 output_port_msg;
void mock_outputCallback(const std_msgs::Int32::ConstPtr& msg)
{
    output_port_msg.data = msg->data;
}

bool mock_fn(oe_msgs::Operation::Request& request, oe_msgs::Operation::Response& response)
{
    (void)request;
    response.value.data = true;

    return true;
}

TEST(SimpleRosTest, EverythingBecauseRosTestDoesntHelpWithRealTesting)
{
    ros::NodeHandle nh;
    sleep(1);  // because ROS is slow AF
    ASSERT_TRUE(nh.ok());

    std::vector<std::string> nodes;
    ros::master::getNodes(nodes);

    bool simple_found = false;
    for (auto const& node : nodes)
    {
        if (node.compare("/simple") == 0)
        {
            simple_found = true;
        }
    }
    ASSERT_TRUE(simple_found);

    // Create some test interaction objects

    std_msgs::Int32 input_port_msg, event_port_msg, event_port_cb_msg;
    ros::Publisher connect_to_input_port = nh.advertise<std_msgs::Int32>("/simple/input_port", 1000);
    ros::Publisher connect_to_event_port = nh.advertise<std_msgs::Int32>("/simple/event_port", 1000);
    ros::Publisher connect_to_event_port_cb = nh.advertise<std_msgs::Int32>("/simple/event_port_cb", 1000);
    ros::Subscriber connect_to_output_port = nh.subscribe<std_msgs::Int32>("/simple/output_port", 1000, mock_outputCallback);

    oe_msgs::Operation operation_msg;
    ros::ServiceClient srv_simple_operation = nh.serviceClient<oe_msgs::Operation>("/simple/simple_operation");
    ASSERT_TRUE(srv_simple_operation.waitForExistence());
    EXPECT_TRUE(srv_simple_operation.exists());
    EXPECT_TRUE(srv_simple_operation.isValid());

    ros::ServiceServer srv_mock = nh.advertiseService("/external/operation", mock_fn);

    int property, input_port_value, event_port_value, event_port_cb_value;
    ASSERT_TRUE(nh.hasParam("/simple/property"));
    ASSERT_TRUE(nh.hasParam("/simple/input_port_value"));
    ASSERT_TRUE(nh.hasParam("/simple/event_port_value"));
    ASSERT_TRUE(nh.hasParam("/simple/event_port_cb_value"));

    nh.getParam("/simple/property", property);
    ASSERT_EQ(0, property);
    nh.getParam("/simple/input_port_value", input_port_value);
    ASSERT_EQ(0, input_port_value);
    nh.getParam("/simple/event_port_value", event_port_value);
    ASSERT_EQ(0, event_port_value);
    nh.getParam("/simple/event_port_cb_value", event_port_cb_value);
    ASSERT_EQ(0, event_port_cb_value);

    // Run some actual tests.
    // SimpleRos components are not configured to allow remote setting of execution rate, so this test will not exactly mirror `simple_test`.

    sleep(1);
    nh.getParam("/simple/property", property);
    EXPECT_EQ(0, property);
    nh.getParam("/simple/input_port_value", input_port_value);
    EXPECT_EQ(0, input_port_value);
    nh.getParam("/simple/event_port_value", event_port_value);
    EXPECT_EQ(0, event_port_value);
    nh.getParam("/simple/event_port_cb_value", event_port_cb_value);
    EXPECT_EQ(0, event_port_cb_value);
    EXPECT_EQ(0, output_port_msg.data);

    nh.setParam("/simple/property", 1);
    sleep(1);
    nh.getParam("/simple/property", property);
    EXPECT_EQ(1, property);
    nh.getParam("/simple/input_port_value", input_port_value);
    EXPECT_EQ(0, input_port_value);
    nh.getParam("/simple/event_port_value", event_port_value);
    EXPECT_EQ(0, event_port_value);
    nh.getParam("/simple/event_port_cb_value", event_port_cb_value);
    EXPECT_EQ(0, event_port_cb_value);
    EXPECT_EQ(1, output_port_msg.data);

    input_port_msg.data = 2;
    connect_to_input_port.publish(input_port_msg);
    sleep(1);
    nh.getParam("/simple/property", property);
    EXPECT_EQ(1, property);
    nh.getParam("/simple/input_port_value", input_port_value);
    EXPECT_EQ(2, input_port_value);
    nh.getParam("/simple/event_port_value", event_port_value);
    EXPECT_EQ(0, event_port_value);
    nh.getParam("/simple/event_port_cb_value", event_port_cb_value);
    EXPECT_EQ(0, event_port_cb_value);
    EXPECT_EQ(3, output_port_msg.data);

    event_port_msg.data = 3;
    connect_to_event_port.publish(event_port_msg);
    sleep(1);
    nh.getParam("/simple/property", property);
    EXPECT_EQ(1, property);
    nh.getParam("/simple/input_port_value", input_port_value);
    EXPECT_EQ(2, input_port_value);
    nh.getParam("/simple/event_port_value", event_port_value);
    EXPECT_EQ(3, event_port_value);
    nh.getParam("/simple/event_port_cb_value", event_port_cb_value);
    EXPECT_EQ(0, event_port_cb_value);
    EXPECT_EQ(6, output_port_msg.data);

    event_port_cb_msg.data = 4;
    connect_to_event_port_cb.publish(event_port_cb_msg);
    sleep(1);
    nh.getParam("/simple/property", property);
    EXPECT_EQ(1, property);
    nh.getParam("/simple/input_port_value", input_port_value);
    EXPECT_EQ(2, input_port_value);
    nh.getParam("/simple/event_port_value", event_port_value);
    EXPECT_EQ(3, event_port_value);
    nh.getParam("/simple/event_port_cb_value", event_port_cb_value);
    EXPECT_EQ(4, event_port_cb_value);
    EXPECT_EQ(10, output_port_msg.data);

    operation_msg.request.value.data = 5;
    EXPECT_TRUE(srv_simple_operation.call(operation_msg));
    EXPECT_TRUE(operation_msg.response.value.data);
    sleep(1);
    nh.getParam("/simple/property", property);
    EXPECT_EQ(1, property);
    nh.getParam("/simple/input_port_value", input_port_value);
    EXPECT_EQ(2, input_port_value);
    nh.getParam("/simple/event_port_value", event_port_value);
    EXPECT_EQ(3, event_port_value);
    nh.getParam("/simple/event_port_cb_value", event_port_cb_value);
    EXPECT_EQ(4, event_port_cb_value);
    EXPECT_EQ(10, output_port_msg.data);
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);

    ros::init(argc, argv, "simple_rostest");
    ros::AsyncSpinner spinner(2);
    spinner.start();

    int ret = RUN_ALL_TESTS();

    spinner.stop();

    return ret;
}
